from django.urls import path

from . import views

app_name = 'web'

urlpatterns = [
    # category
    path('<int:category_id>/', views.category_show, name='category.show'),
    # tech
    path('<int:category_id>/tech/<int:tech_id>', views.tech_show, name="tech.show"),
    # search resources
    path('search', views.search_resources, name="search"),
]
