# Generated by Django 3.0.7 on 2020-06-16 20:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tech',
            old_name='section',
            new_name='category',
        ),
    ]
