from django.contrib import admin
from .models import Category, Tech, Resource

admin.site.register(Category)
admin.site.register(Tech)
admin.site.register(Resource)
