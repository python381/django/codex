from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import Category, Resource
from .models import Tech
from django.contrib.postgres.search import SearchQuery, SearchVector, SearchRank


def home(request):
    return render(request, 'web/home.html')


def search_resources(request):
    search_terms = request.POST['search_terms']
    search_type = request.POST['search_type']

    # create and weight each vector, then combine
    v1 = SearchVector('title', weight='A')
    v2 = SearchVector('description', weight='B')
    v3 = SearchVector('url', weight='C')
    vector = v1 + v2 + v3

    # create the search query
    query = SearchQuery(search_terms, search_type=search_type)
    rank = SearchRank(vector, query)
    search_results = Resource.objects.annotate(rank=rank).filter(rank__gte=0.1).order_by('rank')

    context = {
        'search_results': search_results,
        'search_results_count': search_results.count()
    }
    return render(request, 'web/search/results.html', context)


def category_show(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'web/category/show.html', context)


def tech_show(request, category_id, tech_id):
    tech = get_object_or_404(Tech, pk=tech_id)
    context = { 
        'tech': tech,
        'resource_count': tech.resource_set.count()
    }
    return render(request, 'web/tech/show.html', context)

