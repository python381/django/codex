from django import template
from web.models import Category

register = template.Library()


@register.inclusion_tag('web/partials/navigation/resources.html')
def tech_list():
    categories = Category.objects.all()
    return {'categories': categories}
